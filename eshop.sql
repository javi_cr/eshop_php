﻿CREATE TABLE usuario(
  id serial NOT NULL PRIMARY KEY,
  nombre varchar(20) NOT NULL,
  apellidos varchar(50) NOT NULL,
  telefono varchar NOT NULL,
  email varchar NOT NULL,
  direccion character varying NOT NULL,
  username varchar NOT NULL,
  pass varchar(8) NOT NULL,
  tipo_usuario varchar NOT NULL
)

CREATE TABLE administrador(
  id serial NOT NULL,
  username varchar(14) NOT NULL,
  CONSTRAINT admin_pkey PRIMARY KEY (id)
)

CREATE TABLE categoria(
  id serial NOT NULL,
  nombre varchar NOT NULL,
  categoria_padre varchar,
  CONSTRAINT categoria_pkey PRIMARY KEY (id)
)

CREATE TABLE producto(
  /*SKU, Nombre, Descripción, Imagen, Categoria, Stock, Precio*/
  id integer NOT NULL DEFAULT nextval('producto_id_seq'::regclass),
  sku character varying NOT NULL,
  nombre character varying NOT NULL,
  descripcion character varying NOT NULL,
  imagen character varying NOT NULL,
  categoria integer NOT NULL,
  CONSTRAINT producto_pkey PRIMARY KEY (id),
  CONSTRAINT fk_producto FOREIGN KEY (categoria)
      REFERENCES public.categoria (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_sku UNIQUE (sku)
)