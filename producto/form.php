<form method="POST">
  <div class="form-group">
    <label for="sku">Sku</label>
    <input type="text" class="form-control" placeholder="Sku" name="sku" value="<?=$producto['sku'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" placeholder="Nombre" name="nombre" value="<?=$producto['nombre'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="descripcion">Descripcion</label>
    <input type="text" class="form-control" placeholder="Descripcion" name="descripcion" value="<?=$producto['descripcion'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="imagen">Imagen</label>
    <input type="text" class="form-control" placeholder="img.png" name="imagen" value="<?=$producto['imagen'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="categoria">Categoria</label>
    <input type="text" class="form-control" placeholder="Categoria" name="categoria" value="<?=$producto['categoria'] ?? ''?>" required>
  </div>
  <input class="btn btn-primary" type="submit" value="Aceptar">
  <a class="btn btn-default btn-danger" href="/producto">Cancelar</a>
</form>