<?php
require_once '../shared/guard.php';
$title = 'Agregar Producto';
require_once '../shared/header.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
    $nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
    $descripcion = filter_input(INPUT_POST, 'descripcion', FILTER_SANITIZE_STRING);
    $imagen = filter_input(INPUT_POST, 'imagen', FILTER_SANITIZE_STRING);
    $categoria = filter_input(INPUT_POST, 'categoria', FILTER_SANITIZE_STRING);
    $producto_model->insert($sku, $nombre, $descripcion, $imagen, $categoria);
    return header('Location: /producto');
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once __DIR__ . '/form.php'; ?>
</div>