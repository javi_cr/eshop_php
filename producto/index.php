<?php
  require_once '../shared/guard.php';
  $title = 'Productos';
  require_once '../shared/header.php';
  require_once '../shared/db.php';
  $productos = $producto_model->select();
?>
<div class="container">
  <h1><?=$title?></h1>
  <table class="table table-striped table-bordered">
    <tr>
      <th>Id</th>
      <th>Sku</th>
      <th>Nombre</th>
      <th>Descripcion</th>
      <th>Imagen</th>
      <th>Categoria</th>
      <th class="text-center"><a href="/producto/create.php" class="btn btn-success">+</a></th>
    </tr>
    <?php
      if ($productos) {
          foreach ($productos as $producto) {
              require __DIR__ . '/row.php';
          }
      }
    ?>
  </table>
</div>