<?php
$title = 'Registro';
require_once '../shared/header.php';
require_once '../shared/db.php';

$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
$phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
$direction = filter_input(INPUT_POST, 'direction', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
$tipo_usuario = filter_input(INPUT_POST, 'tipo_usuario', FILTER_SANITIZE_STRING);
$password_verification = filter_input(INPUT_POST, 'password_verification', FILTER_SANITIZE_STRING);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($password == $password_verification) {
        $usuario_model->insert($name, $lastname, $phone, $email, $direction, $username, $password);               
        return header('Location: /seguridad/login.php');
    }
    echo "<h3>Contraseñas no coinciden</h3>";
}
?>
<div class="container">
  <h1><?=$title?></h1>
  <div class="row">
    <div class="col-md-6">
      <form method="POST">
        <div class="form-group">
          <label for="name">Nombre</label>
          <input required="true" type="text" class="form-control" id="name" placeholder="Juan" name="name">
        </div>
        <div class="form-group">
          <label for="lastname">Apellidos</label>
          <input required="true" type="text" class="form-control" id="lastname" placeholder="Castro" name="lastname">
        </div>
        <div class="form-group">
          <label for="phone">Teléfono</label>
          <input required="true" type="text" class="form-control" id="phone" placeholder="87842082" name="phone">
        </div>
        <div class="form-group">
          <label for="direction">Dirección</label>
          <input required="true" type="text" class="form-control" id="direction" placeholder="Barrio Maracana" name="direction">
        </div>
        <div class="form-group">
          <label for="email">Correo</label>
          <input required="true" type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="jncastro@gmail.com" name="email">
        </div>        
        <div class="form-group">
          <label for="username">Nombre de Usuario</label>
          <input required="true" type="text" class="form-control" id="username" placeholder="juancho" name="username">
        </div>
        <div class="form-group">
          <label for="password">Contraseña</label>
          <input required="true" type="password" class="form-control" id="password" placeholder="Password" name="password">
        </div>
        <div class="form-group">
          <label for="password_verification">Verificar</label>
          <input required="true" type="password" class="form-control" id="password_verification" placeholder="Password" name="password_verification">
        </div>
        <div class="form-group">
          <input required="true" type="hidden" class="form-control" id="tipo_usuario" name="tipo_usuario" value="Cliente">
        </div>
        <input required="true" class="btn btn-success" type="submit" value="Guardar">
        <a class="btn btn-danger" href="/seguridad/login.php">Cancelar</a>
      </form>
    </div>
  </div>
</div>


<?php
require_once '../shared/footer.php';