<?php

namespace Models {
    class Producto
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM producto WHERE id = $1', [$id])[0];
        }

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM producto ORDER BY id');
        }

        public function insert($sku, $nombre, $descripcion, $imagen, $categoria )
        {
            $sql = "INSERT INTO producto(sku, nombre, descripcion, imagen, categoria) VALUES ($1, $2, $3, $4, $5)";
            $this->connection->runStatement($sql, [$sku, $nombre, $descripcion, $imagen, $categoria]);
        }

        public function update($id, $sku, $nombre, $descripcion, $imagen, $categoria )
        {
            $this->connection->runStatement("UPDATE producto SET sku=$2, nombre=$3, descripcion=$4, imagen=$5, categoria=$6 WHERE id=$1", [$id, $sku, $nombre, $descripcion, $imagen, $categoria]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM categoria WHERE id = $1', [$id]);
        }
    }
}