<?php

namespace Models {
    class Categoria
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM categoria WHERE id = $1', [$id])[0];
        }

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM categoria ORDER BY id');
        }

        public function insert($nombre, $categoria_padre )
        {
            $sql = "INSERT INTO categoria(nombre, categoria_padre) VALUES ($1, $2)";
            $this->connection->runStatement($sql, [$nombre, $categoria_padre]);
        }

        public function update($id, $nombre, $categoria_padre)
        {
            $this->connection->runStatement("UPDATE categoria SET nombre=$2, categria_padre=$3 WHERE id=$1", [$id, $nombre, $categoria_padre]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM categoria WHERE id = $1', [$id]);
        }
    }
}