<?php
require_once '../shared/guard.php';
$title = 'Agregar Categoría';
require_once '../shared/header.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
    $categoria_padre = filter_input(INPUT_POST, 'categoria_padre', FILTER_SANITIZE_STRING);
    $categoria_model->insert($nombre, $categoria_padre);
    return header('Location: /categoria');
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once __DIR__ . '/form.php'; ?>
</div>