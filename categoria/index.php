<?php
  require_once '../shared/guard.php';
  $title = 'Categorias';
  require_once '../shared/header.php';
  require_once '../shared/db.php';
  $categorias = $categoria_model->select();
?>
<div class="container">
  <h1><?=$title?></h1>
  <table class="table table-striped table-bordered">
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Categoria Padre</th>
      <th class="text-center"><a href="/categoria/create.php" class="btn btn-success">+</a></th>
    </tr>
    <?php
      if ($categorias) {
          foreach ($categorias as $categoria) {
              require __DIR__ . '/row.php';
          }
      }
    ?>
  </table>
</div>