<?php
require_once '../shared/guard.php';
$title = 'Editar Categoria';
require_once '../shared/header.php';
require_once '../shared/db.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$categoria = $categoria_model->find($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $nombre = filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
    $categoria_padre = filter_input(INPUT_POST, 'categoria_padre', FILTER_SANITIZE_STRING);
    $categoria_model->update($id, $nombre, $categoria_padre);
    return header('Location: /categoria');
}
?>
<div class="container">
  <h1><?=$title?></h1>
<?php require_once __DIR__ . '/form.php'; ?>
</div>