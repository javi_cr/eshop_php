<form method="POST">
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" placeholder="Nombre" name="nombre" value="<?=$categoria['nombre'] ?? ''?>" required>
  </div>
  <div class="form-group">
    <label for="categoria_padre">Categoria</label>
    <input type="text" class="form-control" placeholder="Categoria Padre" name="categoria_padre" value="<?=$categoria['categoria_padre'] ?? ''?>">
  </div>
  <input class="btn btn-primary" type="submit" value="Aceptar">
  <a class="btn btn-default btn-danger" href="/categoria">Cancelar</a>
</form>